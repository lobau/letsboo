module.exports = async (route, body) => {
    return `<!DOCTYPE html>
    <html>
    
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
    
        <title>Let's boo</title>
    
        <link rel="preload" href="/Quicksand.ttf" as="font" type="font/ttf" crossorigin />
    
        <link href="/index.css" rel="stylesheet" />

        <script src="/util.js"></script>
        <script type="text/javascript">
            const newRoom = () => {
                window.location = "/" + Util.generateUnique();
            }

        </script>
    
    </head>
    
    <body>
        <div id="placeholderCard">
            <h1>Let's boo!</h1>
            <button onclick="newRoom()">Open a new room</button>
        </div>
    </body>
    
    </html>`;
};

