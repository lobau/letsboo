var isInitialized = false;

const _ = el => document.getElementById(el);

const hueForID = (id) => {
    let hash = 0;
    if (id.length === 0) return 0;
    for (let i = 0; i < id.length; i++) {
        const char = id.charCodeAt(i);
        hash = (hash << 5) - hash + char;
        hash = hash & hash; // Convert to 32bit integer
    }

    const maxValue = Math.pow(2, 31) - 1;
    const hashInt = Math.abs(hash) % (maxValue + 1);

    // Map the hash value to the range [0, 100]
    const result = hashInt % 100;

    // rotate the
    return result * Math.round((5 / 13) * 360);
}

const updateParticipants = () => {
    _("participants").innerHTML = '';

    window.Participants.forEach((participant, index) => {
        participant.stats = {
            stickyCount: 0,
            voteCount: 0
        };

        let cardElements = document.querySelectorAll(".card");
        cardElements.forEach(cardEl => {
            if (participant.id === cardEl.dataset.author) {
                participant.stats.stickyCount++;
            }
        })


        let bubble = document.createElement("div");
        bubble.className = "bubble";

        let hue = hueForID(participant.id);
        // console.log(hue);

        Object.assign(bubble.style, {
            backgroundColor: "hsl(" + hue + ", 100%, 97%)",
            // outline: "2px solid hsl(" + hue + ", 50%, 80%)",
            // outlineOffset: "-2px",
            color: "hsl(" + hue + ", 40%, 40%)",
            boxShadow: "0 0 0 3px white"
        });

        bubble.dataset.name = participant.name;
        bubble.dataset.color = participant.color;
        bubble.innerHTML = participant.name;
        if (window.participant.id === participant.id) {
            bubble.innerHTML += " (You)";
            bubble.classList.add("you");
            // window.participant.index = index;
        }

        // bubble.innerHTML += " #️⃣ " + index;

        // TODO: add sticky and vote counts
        bubble.innerHTML += " • 🟨 " + participant.stats.stickyCount;

        // TODO: Add voting
        // bubble.innerHTML += " 👍️ " + Math.round((Math.random() * 10) + 2);

        _("participants").appendChild(bubble);
    });
}

class CardsCollection {
    constructor() {
        this.Cards = [];
    }
    addCard = (card) => {
        this.Cards.push(card);
    };
}

class TextCard {
    constructor(string, id, participant) {
        this.string = string;
        this.id = id;
        this.participant = participant;

        this.view = document.createElement("div");
        this.view.className = "card";
        this.view.dataset.author = participant.id;

        Object.assign(this.view.style, {
            backgroundColor: "hsl(" + hueForID(this.participant.id) + ", 80%, 95%)",
            color: "hsl(" + hueForID(this.participant.id) + ", 40%, 40%)",
            // border: "1px solid hsl(" + this.participant.hue + ", 20%, 90%)"
        });
        this.view.id = this.id;
        // this.view.amId = this.id;
        this.view.draggable = true;
        document.getElementById("masonry").appendChild(this.view);

        // this.view.innerHTML =

        this.content = document.createElement("div");
        this.content.className = "textContent";

        // TODO: make the parsing and decoding of ** and * here

        let htmlContent = this.string.replace(/(?:\r\n|\r|\n)/g, "<br />");

        // Important
        htmlContent = htmlContent.replace(/\*\*(.*?)\*\*/g, "<strong>$1</strong>");

        // Emphasized
        htmlContent = htmlContent.replace(/\*(.*?)\*/g, "<em>$1</em>");

        this.content.innerHTML = htmlContent;


        let remSize = 8 / this.string.length + 1;
        this.content.style.fontSize = remSize + "rem";
        this.content.style.fontWeight = Math.round(600 - remSize * 100);
        this.content.style.lineHeight = "130%";
        this.view.appendChild(this.content);

        this.bottomView = document.createElement("div");
        this.bottomView.className = "bottomView";
        Object.assign(this.bottomView.style, {
            backgroundColor: "hsl(" + this.participant.hue + ", 100%, 90%)",
        });
        // this.bottomView.style.
        this.view.appendChild(this.bottomView);

        this.author = document.createElement("author");
        this.author.innerText = this.participant.name;
        if (this.participant.id === window.participant.id) {
            this.author.innerText += " (You)";
        }
        this.bottomView.appendChild(this.author);

        if (this.participant.id === window.participant.id || this.participant.id == 0) {

            this.deleteButton = document.createElement("div");
            this.deleteButton.className = "deleteButton";
            this.deleteButton.innerText = "delete";
            this.bottomView.appendChild(this.deleteButton);

            this.deleteButton.addEventListener(
                "click",
                () => {
                    socket.emit("delete card", this.id, window.route);
                })
        }
    };
}

window.onload = () => {

    // Store the ID in the localstorage so it's persistent through reloads
    if (localStorage.getItem("participantID")) {
        window.participantID = localStorage.getItem("participantID");
    } else {
        window.participantID = Util.generateUnique();
        localStorage.setItem("participantID", window.participantID);
    }

    // Store the Name in the localstorage so it's persistent through reloads
    if (localStorage.getItem("participantName")) {
        window.participantName = localStorage.getItem("participantName");
    } else {
        window.participantName = Util.randomName();
        localStorage.setItem("participantName", window.participantName);
    }

    var Cards = new CardsCollection();

    socket.on('connect', () => {

        window.participant = {
            id: window.participantID,
            name: window.participantName,
            // hue: window.participantHue,
            socketid: socket.id
        }

        socket.emit("register participant", window.participant, window.route);

        const grid = document.body.querySelector('#masonry');
        let items = Array.from(document.body.querySelectorAll('.card'));

        function move(from, to) {
            const isLeft = from > to;

            items = Array.from(document.body.querySelectorAll('.card'));
            const draggedItem = items[from];

            grid.insertBefore(draggedItem, isLeft ? items[to] : items[to + 1]);
        };

        let dragged = null
        grid.addEventListener('dragstart', e => {
            dragged = e.composedPath()[0];
        })

        grid.addEventListener('dragover', e => {
            e.preventDefault();
            if (!dragged) return;

            const dragoverItem = e.composedPath().find(a => a && a.classList && a.classList.contains('card') && a !== dragged && !a.classList.contains('animating'));
            if (!dragoverItem) return;
            const to = Array.from(document.body.querySelectorAll('.card')).indexOf(dragoverItem);
            const from = Array.from(document.body.querySelectorAll('.card')).indexOf(dragged);

            if (from === to || from === -1 || to === -1) return;

            move(from, to);
        })

        grid.addEventListener('dragend', e => {
            dragged = null;
            console.log(e);

            // TODO: send the new order to the server and reorder on all clients
        })


        window.lastScroll = 0;
        window.addEventListener("scroll", () => {
            if (window.scrollY <= 0) {
                _("toolbar").classList.remove("toolbar-shadow");
            } else {
                _("toolbar").classList.add("toolbar-shadow");
            }
            window.lastScroll = window.scrollY;
        });
    })

    socket.on("seed cards", function(InitialCards) {
        if (!isInitialized) {
            var card;
            for (var i = 0; i < InitialCards.length; i++) {
                card = new TextCard(InitialCards[i].text, InitialCards[i].id, InitialCards[i].participant);
                Cards.addCard(card);
            }
        }
        isInitialized = true;
    });


    socket.on("update participants", Participants => {
        window.Participants = Participants;

        _("participants").innerHTML = "";

        // For overlapping bubbles in the UI
        // the flexbox is set to `flex-direction: row-reverse;`
        // This restore the original order.
        window.Participants.reverse();
        updateParticipants();

        let yourHue = hueForID(window.participant.id);

        _("inputAdd").style.setProperty("--background-color", "hsl(" + yourHue + ", 80%, 90%)");
        _("inputAdd").style.setProperty("--shadow-color", "hsla(" + yourHue + ", 100%, 20%, 0.2)");
        _("inputAdd").style.setProperty("--border-color", "hsl(" + yourHue + ", 50%, 85%)");
        _("inputAdd").style.setProperty("--border-color-focus", "hsl(" + yourHue + ", 50%, 70%)");
        _("inputAdd").style.setProperty("--placeholder", "hsl(" + yourHue + ", 70%, 40%)");
        _("inputAdd").style.setProperty("--ink", "hsl(" + yourHue + ", 20%, 20%)");
    });

    socket.on("text card", (msg, id, participant) => {
        // console.log(participant);
        Cards.addCard(new TextCard(msg, id, participant));
        updateParticipants();
    });

    socket.on("delete card", id => {

        _("masonry").removeChild(_(id));

        // Below code is for the poof
        // var position = _(id).getBoundingClientRect();
        // var x = position.left;
        // var y = position.top + window.scrollY;
        // var w = position.width;
        // var h = position.height;
        //
        // console.log(x, y, w, h);
        //
        // let poof = document.createElement("div");
        // poof.className = "poof";
        // poof.innerHTML = "TODO: Poof animation";
        // Object.assign(poof.style, {
        //     position: "absolute",
        //     left: Math.round(x - 50) + "px",
        //     top: Math.round(y - 50) + "px",
        //     width: Math.round(w + 100) + "px",
        //     height: Math.round(h + 100) + "px",
        //     backgroundColor: "rgba(255, 0, 0, 0.5)"
        // });
        // document.body.appendChild(poof)
        //
        // setTimeout(() => {
        //     _("masonry").removeChild(_(id));
        //
        //     var poofs = document.getElementsByClassName('poof');
        //
        //     while (poofs[0]) {
        //         poofs[0].parentNode.removeChild(poofs[0]);
        //     }
        //
        //     updateParticipants();
        // }, 300);

    });

    _("inputAdd").addEventListener("keyup", function(event) {
        var data = event.target.value.replace(/\n+$/, "");

        if (event.keyCode == 13 && data != "" && !event.shiftKey) {

            socket.emit(
                "post card",
                data,
                Util.generateUnique(),
                window.participant,
                window.route
            );

            this.value = "";
            event.preventDefault();
        }
    });
}
