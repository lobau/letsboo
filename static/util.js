const Util = {
    generateUnique: (length = 24) => {
        let chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        let str = "";
        for (let i = 0; i < length; i++) {
            str += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return str;
    },
    lorem: (wordCount) => {

        // collection of random words
        const words = "lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua velit egestas dui id ornare arcu odio ut sem nulla lorem sed risus ultricies tristique nulla aliquet enim tortor at nibh sed pulvinar proin gravida hendrerit lectus a risus sed vulputate odio ut enim cursus euismod quis viverra nibh cras pulvinar quis enim lobortis scelerisque fermentum dui faucibus in ornare dictumst vestibulum rhoncus est pellentesque elit blandit cursus risus at ultrices mi tempus nulla pharetra diam sit amet nisl suscipit adipiscing";

        // create an array and shuffle it
        const shuffled = words.split(" ");
        for (let i = shuffled.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [shuffled[i], shuffled[j]] = [shuffled[j], shuffled[i]];
            if (Math.random() > 0.8) {
                if (Math.random() > 0.5) {
                    shuffled[i] = "**" + shuffled[i] + "**";
                } else {
                    shuffled[i] = "*" + shuffled[i] + "*";
                }

            }
            if (Math.random() > 0.8) {
                shuffled[i] = shuffled[i].toUpperCase();
            }
        }

        // trim and join the result
        let str = shuffled.slice(0, wordCount).join(" ");

        // capitalize the first letter
        return str.charAt(0).toUpperCase() + str.slice(1);
    },
    randomName: () => {

        // collection of random words

        const colorsString = "Cherry Strawberry Chocolate Raspberry Boysenberry Blueberry Peach Blackberry Pineapple Coconut Custard Banana Persimmon Guava Cranberry Plum Grape Plum Kiwifruit Honeyberry Honeysuckle Huckleberry Plantain Pomegranate Pear Apple Lemon Orange Tangerine Watermelon Cantaloupe Honeydew Lime Cloudberry Loganberry Fig Rhubarb Peanut Walnut Almond Hazelnut Mocha Apricot Cashew Pistachio Mint";

        // const colorsString = "red green blue yellow orange tangerine mauve purple pink";
        const animalsString = "Pie Parfait Cake Tart Cupcake Cookie Soufflé Cheesecake Pudding Brownie Torte Bonbon Cordial Truffle Fudge Scone Éclair Cake Crêpe Jam Sorbet";

        const Colors = colorsString.split(" ");
        const Animals = animalsString.split(" ");

        const randomColor = Colors[Math.floor(Math.random() * Colors.length)];
        const randomAnimal = Animals[Math.floor(Math.random() * Animals.length)];

        return randomColor.charAt(0).toUpperCase() + randomColor.slice(1) + " " + randomAnimal;
    }
}

