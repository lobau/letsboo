const express = require('express');
const path = require("path");
const app = express();
const http = require("http").Server(app);
const port = Number(process.env.PORT) || 8080;
const io = require("socket.io")(http);
const publicPath = path.resolve(__dirname, 'static');

const route_landing = require("./route_landing.js");
const route_app = require("./route_app.js");

const Cards = {}
const Participants = {};

app.use(express.static(publicPath));

// TODO: add disk to persist data

app.get('/', (req, res) => {
    route_landing().then(
        function(html) {
            res.writeHead(200, {
                "Content-Type": "text/html"
            });
            res.write(html);
            res.end();
        }.bind(res)
    );
})

app.get('/:route([a-zA-Z0-9]{24})/', (req, res) => {

    // TODO: Add persistent storage
    // check if a file exists
    // if the file doesn't exists: create a file and seed it with onboarding data
    // otherwise, load the file

    // pass the data to the page, probably pass (route, DATA) to the route_app

    const route = req.params.route;
    route_app(route).then(
        html => {
            res.writeHead(200, {
                "Content-Type": "text/html"
            });
            res.write(html);
            res.end();
        }
    );
});

http.listen(port, () => {
    console.log(`listening on http://localhost:${port}`);
});

io.sockets.on("connection", (socket) => {

    socket.on("post card", (msg, id, participant, route) => {

        if (!Cards[route]) Cards[route] = [];

        io.to(route).emit("text card", msg, id, participant);
        Cards[route].push({
            id: id,
            text: msg,
            participant: participant
        });
        // TODO: write change to file
    });

    socket.on("delete card", (id, route) => {
        io.sockets.in(route).emit("delete card", id);
        Cards[route] = Cards[route].filter(function(card) {
            return card.id !== id;
        });
        // TODO: write change to file
    });

    socket.on("register participant", (participant, route) => {
        // Filter to add only new participants
        let doesExist = false;
        socket.join(route);


        if (!Participants[route]) {
            Participants[route] = [];
        }

        if (!Cards[route]) {
            Cards[route] = []
        }

        Participants[route].forEach(record => {
            if (record.id === participant.id) {
                doesExist = true;

                // Update the participant with new socket ID
                record.socketid = socket.id;
            }
        });

        if (!doesExist) {
            Participants[route].push(participant);
        }

        Participants[route] = Participants[route].sort((a, b) => {
            // This is an alphabetical sort because the ids are random hash and this is the most stable list I found, UX-wise.
            return a.id < b.id ? -1 : a.id > b.id ? 1 : 0;
        });

        io.sockets.in(route).emit("seed cards", Cards[route]);
        io.sockets.in(route).emit("update participants", Participants[route]);
    });

    socket.on("disconnect", () => {
        Object.keys(Participants).forEach(route => {

            Participants[route].forEach(participant => {
                if (participant.socketid == socket.id) {
                    Participants[route] = Participants[route].filter(record => {
                        return record.socketid !== socket.id;
                    });

                    io.sockets.in(route).emit("update participants", Participants[route]);
                }
            })
        });
    });
});