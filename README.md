# Let's boo

A barebone real-time sticky note board that supports text, including image and link previews, and youtube links, which has synchronized playback between all participants.

Not stable or feature complete. Don't use :)
Check out the TODO.txt file if you want to see where I am at.

---

## UX Flow / Technical thoughts
- Sign in with Dropbox. Client-side auth, store token in localstorage. No backend at all for this.
- Empty list of boards (query of the dropbox app folder for .boo files)
- Click "Create new board"
- Create a new file in the folder. The file is called "Board created on [date].json".
- Redirection to a [lets.boo/session_id]. The session_id is a random unique identifier associated to a file. The session_id is not persistents across server reboots, so no mapping from dropbox documents to urls is needed.
- If the user instead opens an existing file, the server creates a random route, and loads the content of the file in the socket. Caveat: Only the owner of the document has the token to save the file in dropbox. So the lets.boo/session_id page has to know who is the owner of the document.
- Can it uses WebRTC instead of socket.io so it doesn't even need a backend? 