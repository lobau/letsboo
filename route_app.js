module.exports = async (route) => {
    return `<!DOCTYPE html>
    <html>
    
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
    
        <title>Let's boo</title>
    
        <link rel="preload" href="/Quicksand.ttf" as="font" type="font/ttf" crossorigin />
    
        <link href="/index.css" rel="stylesheet" />
        <script src="/socket.io/socket.io.js"></script>
        <script type="text/javascript">
             var socket = io.connect('', { query: 'route=${route}'});
             window.route = "${route}";
        </script>

        <script src="/util.js"></script>
        <script src="/index.js"></script>
    
    </head>
    
    <body>
    
        <div id="toolbar">
            <div id="participants">
                <div class="bubble">—</div>
            </div>
            <div id="tools">
                <!--<button onclick="updateParticipants()">Export</button>-->
            </div>
        </div>
    
    
        <div id="masonry" class="masonry-container">
            <div id="placeholderCard">
                <h1>That's it!</h1>
                <p>Now share this page link and start brainstorming!</p>
            </div>
        </div>
    
        <div id="bottomGradient"></div>
        <textarea id="inputAdd" name="inputString" placeholder="Share a thought"></textarea>
    
    </body>
    
    </html>`;
};
